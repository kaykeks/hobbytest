/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    dashboard: function(req,res){
        req.session.loginError=" ";
        console.log(req.session.userId);
        data= {
            id:req.session.userId
        }
        User.findOne(data).populateAll().exec(function(err, user){
            if (err){
                console.log(err);
                return;
            }  
            res.view(
                {users:user}); 
        })
        
    },
    register: function(req,res){
        req.session.loginError=" ";
        res.view();
        
    },

	processregister:function(req, res){
        data = {
            name:req.body.name,
            username:req.body.username,
            password:req.body.password,
            phone:req.body.phone,
            email:req.body.email
        }
 
        User.create(data).exec(function(err, user){
            if (err){
                res.send(500, {error: 'Database Error'});
                return;
            }
            if(!user.username){
                res.redirect('/user/register');
                return;
            }
            req.session.userId=user.id;
            req.session.name=user.name;
            req.session.email=user.email;
            req.session.phone=user.phone;
            res.redirect('/user/dashboard');
            

            
        })
    },

    login: function(req,res){
        res.view();

    },
 


    processlogin:function(req,res){
        data = {
            username:req.body.username,
            password:req.body.password
        }
        User.findOne(data).then(function(user){
            if(!user) {
                req.session.loginError="User does not exist";
                res.redirect('/user/login');

                return;
            };
            if(!user.username){
                res.redirect('/user/login');
                return;
            }
            req.session.userId=user.id;
            console.log(req.session.userId);
            req.session.name=user.name;
            req.session.email=user.email;
            req.session.phone=user.phone;
            
            res.redirect('/user/dashboard');
        })  

    },
    homepage: function(req,res){
        req.session.loginError=" ";
        res.view();
        

    }
 
};

